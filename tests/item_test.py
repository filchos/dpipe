import copy
import logging
import pytest
from typing import Callable
import unittest

from kojo.item import Item, ItemDecoder, ItemEncoder, ItemLog, ItemLogEntry


class TestItemLogEntry(unittest.TestCase):
    def test(self) -> None:
        entry = ItemLogEntry(logging.INFO, "Hei", reason="Testing")
        self.assertEqual(entry.level, logging.INFO)
        self.assertEqual(entry.message, "Hei")
        self.assertEqual(entry.details["reason"], "Testing")
        self.assertEqual(entry["reason"], "Testing")
        self.assertTrue("reason" in entry)
        self.assertEqual(
            repr(entry), "ItemLogEntry(logging.INFO, 'Hei', reason='Testing')"
        )


class TestItemLog(unittest.TestCase):
    def test_new_log(self) -> None:
        log = ItemLog()
        self.assertEqual(len(log), 0)
        self.assertEqual(log.level, logging.NOTSET)

    def test_ascending_log_level(self) -> None:
        log = ItemLog()

        log.debug("D")
        self.assertEqual(len(log), 1)
        self.assertEqual(log[-1].message, "D")
        self.assertEqual(log[-1].level, logging.DEBUG)

        log.info("I")
        self.assertEqual(len(log), 2)
        self.assertEqual(log[-1].message, "I")
        self.assertEqual(log[-1].level, logging.INFO)

        log.warning("W")
        self.assertEqual(len(log), 3)
        self.assertEqual(log[-1].message, "W")
        self.assertEqual(log[-1].level, logging.WARNING)

        log.error("E")
        self.assertEqual(len(log), 4)
        self.assertEqual(log[-1].message, "E")
        self.assertEqual(log[-1].level, logging.ERROR)

        log.critical("C")
        self.assertEqual(len(log), 5)
        self.assertEqual(log[-1].message, "C")
        self.assertEqual(log[-1].level, logging.CRITICAL)

    def test_copy(self) -> None:
        log = ItemLog()
        log.debug("D")
        log.info("I")

        log2 = copy.copy(log)
        log.warning("W")  # shall not change log2

        self.assertEqual(len(log2), 2)
        self.assertEqual(log2[0].message, "D")
        self.assertEqual(log2[1].message, "I")
        self.assertEqual(log2.level, logging.INFO)

    def test_repr(self) -> None:
        log = ItemLog()

        self.assertEqual(repr(log), "ItemLog()")

        log.debug("First")
        log.info("Second")

        exp = "ItemLog(ItemLogEntry(logging.DEBUG, 'First'), ItemLogEntry(logging.INFO, 'Second'))"
        self.assertEqual(repr(log), exp)

    def test_iterable(self) -> None:
        log = ItemLog()
        msg = ["Debug", "Info"]
        log.debug(msg[0])
        log.info(msg[1])

        for i, entry in enumerate(log):
            self.assertEqual(entry.message, msg[i])

    def test_level(self) -> None:
        log = ItemLog()
        log.debug("D")
        self.assertEqual(log.level, logging.DEBUG)
        log.error("E")
        self.assertEqual(log.level, logging.ERROR)
        log.info("I")
        self.assertEqual(log.level, logging.ERROR)

    def test_add(self) -> None:
        log1 = ItemLog()
        log1.debug("D")
        log2 = ItemLog()
        log2.warning("W")
        log2.info("I")
        log = log1 + log2
        self.assertEqual(log.level, logging.WARNING)
        self.assertEqual(len(log1), 1)
        self.assertEqual(len(log2), 2)
        self.assertEqual(len(log), 3)
        self.assertEqual(log[0].message, "D")
        self.assertEqual(log[1].message, "W")
        self.assertEqual(log[2].message, "I")

    def test_add_with_non_itemlog_instance(self) -> None:
        with pytest.raises(TypeError):
            ItemLog() + []


class TestItem(unittest.TestCase):
    def test_equality_with_dict(self) -> None:
        data = {"star": "Sun", "planet": "Earth", "position": 3}
        item = Item(data)
        self.assertEqual(item, data)

    def test_copy_and_deepcopy(self) -> None:
        modes: list[tuple[str, Callable[[Item], Item]]] = [
            ("copy", copy.copy),
            ("deepcopy", copy.deepcopy),
        ]

        for mode, method in modes:
            item = Item(star="Sun", planet={"name": "Earth", "position": 3})
            item.meta["index"] = 1
            item.log.debug("A log entry")

            dolly = method(item)

            # check data integrity
            self.assertEqual(dolly["star"], "Sun")
            self.assertDictEqual(item["planet"], dolly["planet"])

            # check data copy mode
            if mode == "copy":
                self.assertEqual(id(item["planet"]), id(dolly["planet"]))
            else:
                self.assertNotEqual(id(item["planet"]), id(dolly["planet"]))

            # check data independency

            dolly["star"] = "Regulus"
            self.assertEqual(item["star"], "Sun")
            self.assertEqual(dolly["star"], "Regulus")

            item["new"] = True
            self.assertNotIn("new", dolly)

            # check log entry integrity

            self.assertEqual(len(dolly.log), 1)
            self.assertEqual(dolly.log[0].message, "A log entry")

            # check log entry independency

            item.log.debug("Another log entry")
            self.assertEqual(len(dolly.log), 1)

            # check meta data integrity
            self.assertEqual(dolly.meta["index"], 1)

            # check meta data independency

            item.meta["additional"] = True
            self.assertNotIn("additional", dolly.meta)

    def test_deep_copy(self) -> None:
        item = Item(star="Sun", planet={"name": "Earth", "position": 3})
        item.meta["index"] = 1
        item.log.debug("A log entry")

        dolly = copy.deepcopy(item)

        self.assertDictEqual(item["planet"], dolly["planet"])
        self.assertNotEqual(id(item["planet"]), id(dolly["planet"]))

    def test_flatten(self) -> None:
        item = Item({"id": 6174})
        item.meta["index"] = 1
        item.log.debug("A log entry")

        flat = item.flatten()
        self.assertEqual(flat["id"], 6174)
        self.assertEqual(len(flat.log), 1)
        self.assertEqual(flat.log[0].message, "A log entry")
        self.assertEqual(flat.meta["index"], 1)

        item["sub"] = {"mol": 42, "subsub": {"pole": 90}}
        flat = item.flatten()
        self.assertNotIn("sub", flat)
        self.assertEqual(flat["sub.mol"], 42)
        self.assertEqual(flat["sub.subsub.pole"], 90)
        self.assertEqual(len(flat.log), 1)
        self.assertEqual(flat.log[0].message, "A log entry")
        self.assertEqual(flat.meta["index"], 1)

    def test_flatten_with_nested_item(self) -> None:
        subitem = Item(id=2)
        item = Item(id=1, subitem=subitem)

        flat = item.flatten()
        self.assertDictEqual(dict(flat), {"id": 1, "subitem.id": 2})


class TestItemEncoder(unittest.TestCase):
    def test(self) -> None:
        item = Item(id=6174)
        item.meta["scope"] = "test"
        item.log.debug("debug message", comment="debug")
        item.log.info("info message", comment="info")

        data = ItemEncoder().encode(item)

        del item

        self.assertIsInstance(data, str)

        item = ItemDecoder().decode(data)

        self.assertEqual(item.get("id"), 6174)
        self.assertEqual(item.meta.get("scope"), "test")
        self.assertEqual(len(item.log), 2)
        self.assertEqual(item.log.level, logging.INFO)
        entry = item.log[0]
        self.assertEqual(entry.level, logging.DEBUG)
        self.assertEqual(entry.message, "debug message")
        self.assertEqual(entry.details.get("comment"), "debug")
        entry = item.log[1]
        self.assertEqual(entry.level, logging.INFO)
        self.assertEqual(entry.message, "info message")
        self.assertEqual(entry.details.get("comment"), "info")
