import itertools
import json
from typing import Iterator
import unittest

from kojo.item import Item
from kojo.process import (
    MapStep,
    MapStepFunction,
    Process,
    ProcessEncoder,
    StepEncoder,
    apply,
    apply_on_item,
)


# example generator


def example_generator() -> Iterator[Item]:
    for prime in [2, 3, 5, 7, 11]:
        yield Item({"n": prime})


# example transformers


def square(item: Item) -> Item:
    item["n"] = item["n"] ** 2
    return item


def increase(item: Item) -> Item:
    item["n"] = item["n"] + 1
    return item


# example filters


def isodd(item: Item) -> bool:
    return bool(item["n"] % 2 == 1)


def isonedigit(item: Item) -> bool:
    return bool(item["n"] < 10)


class TestPipeline(unittest.TestCase):
    def test_blankio(self) -> None:
        process = Process()
        iterator = apply(example_generator(), process)

        self.assertTrue(hasattr(iterator, "__next__"))
        self.assertTrue(hasattr(iterator, "__iter__"))
        self.assertTrue(callable(iterator.__iter__))
        self.assertTrue(iterator.__iter__() is iterator)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [2, 3, 5, 7, 11])

    def test_reuse_process(self) -> None:
        process = Process()

        iterator = apply(example_generator(), process)
        list(iterator)

        iterator = apply(example_generator(), process)
        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [2, 3, 5, 7, 11])

    def test_map(self) -> None:
        process = Process().map(square).map(increase)
        iterator = apply(example_generator(), process)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [5, 10, 26, 50, 122])

    def test_filter(self) -> None:
        process = Process().filter(isodd).filter(isonedigit)
        iterator = apply(example_generator(), process)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [3, 5, 7])

    def test_filter_and_map(self) -> None:
        process = Process().filter(isodd).map(increase).filter(isonedigit).map(square)
        iterator = apply(example_generator(), process)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [16, 36, 64])

    def test_filter_and_map_in_two_processes(self) -> None:
        process1 = Process().filter(isodd).map(increase)
        process2 = Process().filter(isonedigit).map(square)
        iterator = apply(example_generator(), process1, process2)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [16, 36, 64])

    def test_filter_and_map_in_two_apply_calls(self) -> None:
        process1 = Process().filter(isodd).map(increase)
        process2 = Process().filter(isonedigit).map(square)
        iterator = apply(apply(example_generator(), process1), process2)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [16, 36, 64])

    def test_generator_only_once(self) -> None:
        process = Process().filter(isodd).map(increase).filter(isonedigit).map(square)
        iterator = apply(example_generator(), process)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [16, 36, 64])

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [])

    def test_use_tee(self) -> None:
        process = Process().filter(isodd).map(increase).filter(isonedigit).map(square)
        iterator = apply(example_generator(), process)

        iter1, iter2 = itertools.tee(iterator, 2)

        numbers = [item["n"] for item in iter1]
        self.assertEqual(numbers, [16, 36, 64])

        numbers = [item["n"] for item in iter2]
        self.assertEqual(numbers, [16, 36, 64])

    def test_call(self) -> None:
        process = Process().filter(isodd).map(increase).filter(isonedigit).map(square)
        iterator = process(example_generator())

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [16, 36, 64])

    def test_apply_on_item(self) -> None:
        process = Process().filter(isodd).map(increase).filter(isonedigit).map(square)

        passing = [1, 3]
        for n in passing:
            want = (n + 1) ** 2
            item = apply_on_item(Item(n=n), process)
            self.assertIsNotNone(item)
            if item is not None:
                self.assertEqual(item["n"], want)

        failing = [
            2,  # fail on isOdd,
            9,  # fail on isonedigit
        ]
        for n in failing:
            item = apply_on_item(Item(n=n), process)
            self.assertIsNone(item)

    def test_iadd_operator(self) -> None:
        process = Process()
        process += increase
        process += square

        iterator = apply(example_generator(), process)
        processed = list(iterator)

        self.assertEqual(len(processed), 5)
        self.assertEqual(processed[4]["n"], 144)

    def test_imul_operator(self) -> None:
        process = Process()
        process *= isodd
        process *= isonedigit

        iterator = apply(example_generator(), process)
        processed = list(iterator)

        self.assertEqual(len(processed), 3)
        self.assertEqual(processed[0]["n"], 3)
        self.assertEqual(processed[2]["n"], 7)


# BEGIN STEP FUNCTION IMPLEMENTATIONS
# To get clean names, we avoid defining step functions in a function of a test
# class.


class AClassWithAMapStepFunction:
    def map23(self, item: Item) -> Item:
        """For testing"""
        return item


def map31(item: Item) -> Item:
    """For testing"""
    return item


class ACallableMapStepFunction:
    def __call__(self, item: Item) -> Item:
        """For testing"""
        return item


class ACallableMapStepFunctionWithClassDocString:
    """For testing"""

    def __call__(self, item: Item) -> Item:
        return item


class ACallableMapStepFunctionWithoutDocString:
    def __call__(self, item: Item) -> Item:
        return item


def filter37(item: Item) -> bool:
    """For testing"""
    return type(item) is Item


map41 = lambda item: item  # noqa: E731


def map43(item: Item) -> Item:
    """
     For
    testing
    """
    return item


def create_map47() -> MapStepFunction:
    def map47(item: Item) -> Item:
        """For testing"""
        return item

    return map47


# END STEP FUNCTION IMPLEMENTATIONS


class TestStepEncoder(unittest.TestCase):
    def test_map_function(self) -> None:
        step = MapStep(map31)

        obj = json.loads(StepEncoder().encode(step))
        expected = {
            "description": "For testing",
            "module": __name__,
            "name": "map31",
            "type": "MapStep",
        }
        self.assertDictEqual(obj, expected)

    def test_map_function_with_factory_function(self) -> None:
        step = MapStep(create_map47())

        obj = json.loads(StepEncoder().encode(step))
        expected = {
            "description": "For testing",
            "module": __name__,
            "name": "map47",
            "type": "MapStep",
        }
        self.assertDictEqual(obj, expected)

    def test_map_function_with_multiline_docstring(self) -> None:
        step = MapStep(map43)

        obj = json.loads(StepEncoder().encode(step))
        expected = {
            "description": "For testing",
            "module": __name__,
            "name": "map43",
            "type": "MapStep",
        }
        self.assertDictEqual(obj, expected)

    def test_map_method(self) -> None:
        step = MapStep(AClassWithAMapStepFunction().map23)

        obj = json.loads(StepEncoder().encode(step))
        expected = {
            "description": "For testing",
            "module": __name__,
            "name": "AClassWithAMapStepFunction.map23",
            "type": "MapStep",
        }
        self.assertDictEqual(obj, expected)

    def test_map_class(self) -> None:
        step = MapStep(ACallableMapStepFunction())

        obj = json.loads(StepEncoder().encode(step))
        expected = {
            "description": "For testing",
            "module": __name__,
            "name": "ACallableMapStepFunction",
            "type": "MapStep",
        }
        self.assertDictEqual(obj, expected)

    def test_map_class_with_class_docstring(self) -> None:
        step = MapStep(ACallableMapStepFunctionWithClassDocString())

        obj = json.loads(StepEncoder().encode(step))
        expected = {
            "description": "For testing",
            "module": __name__,
            "name": "ACallableMapStepFunctionWithClassDocString",
            "type": "MapStep",
        }
        self.assertDictEqual(obj, expected)

    def test_map_class_with_dynamic_docstring(self) -> None:
        fn = ACallableMapStepFunctionWithoutDocString()
        fn.__doc__ = "Dynamic doc string"
        step = MapStep(fn)

        obj = json.loads(StepEncoder().encode(step))
        expected = {
            "description": "Dynamic doc string",
            "module": __name__,
            "name": "ACallableMapStepFunctionWithoutDocString",
            "type": "MapStep",
        }
        self.assertDictEqual(obj, expected)


class TestProcessEncoder(unittest.TestCase):
    def test_empty_process(self) -> None:
        p = Process()

        obj = json.loads(ProcessEncoder().encode(p))

        self.assertIsInstance(obj, list)
        self.assertEqual(len(obj), 0)

    def test_a_named_function(self) -> None:
        p = Process()

        p += map31
        obj = json.loads(ProcessEncoder().encode(p))

        self.assertEqual(len(obj), 1)
        expected = {
            "description": "For testing",
            "module": __name__,
            "name": "map31",
            "type": "MapStep",
        }
        self.assertDictEqual(obj[0], expected)

    def test_a_named_filter_function(self) -> None:
        p = Process()

        p *= filter37
        obj = json.loads(ProcessEncoder().encode(p))

        self.assertEqual(len(obj), 1)
        expected = {
            "description": "For testing",
            "module": __name__,
            "name": "filter37",
            "type": "FilterStep",
        }
        self.assertDictEqual(obj[0], expected)

    def test_a_lambda_map_function(self) -> None:
        # using lambda functions as steps is discouraged. Linters will complain
        # and it is not possible to retrieve a name or a description.
        p = Process()

        p += map41
        obj = json.loads(ProcessEncoder().encode(p))

        self.assertEqual(len(obj), 1)
        expected = {
            "description": None,
            "module": __name__,
            "name": "<lambda>",
            "type": "MapStep",
        }
        self.assertDictEqual(obj[0], expected)
