import csv
import os
import pytest
import tempfile
from typing import Any, Iterator
import unittest

from kojo.extra import TabularReader


class TestTabularReader(unittest.TestCase):
    def test(self) -> None:
        inner_iterator: Iterator[list[Any]] = iter(
            [
                ["planet", "numberOfMoons"],
                ["Mercury", 0],
                ["Venus", 0],
                ["Earth", 1],
            ]
        )

        reader = TabularReader(inner_iterator)

        self.assertEqual(reader.headers, ["planet", "numberOfMoons"])
        self.assertIsInstance(reader, Iterator)

        data = list(reader)
        self.assertEqual(len(data), 3)
        self.assertEqual(data[0], {"planet": "Mercury", "numberOfMoons": 0})
        self.assertEqual(data[2], {"planet": "Earth", "numberOfMoons": 1})

    def test_with_surplus_cells(self) -> None:
        inner_iterator: Iterator[list[Any]] = iter(
            [
                ["planet", "numberOfMoons"],
                ["Mercury", 0, "a Roman god"],
            ]
        )

        reader = TabularReader(inner_iterator)

        with pytest.raises(IndexError):
            list(reader)

    def test_with_missing_cells(self) -> None:
        inner_iterator = iter(
            [
                ["planet", "numberOfMoons", "comment"],
                ["Mercury"],
            ]
        )

        reader = TabularReader(inner_iterator, default_cell_value="n/a")
        data = list(reader)
        self.assertEqual(len(data), 1)
        self.assertEqual(
            data[0], {"planet": "Mercury", "numberOfMoons": "n/a", "comment": "n/a"}
        )

    def test_with_csv(self) -> None:
        """
        This tests acts as an example on how to read tabular data from a file.
        """

        with tempfile.TemporaryDirectory() as tempdir:
            path = os.path.join(tempdir, "kojo-unittest.tsv")

            # write test file
            content = "planet\tnumberOfMoons\nMercury\t0\nVenus\t0\nEarth\t1"
            with open(path, "w") as fw:
                fw.write(content)

            # test TabularReader together with csv.reader

            with open(path, "r") as fr:
                inner_iterator = csv.reader(fr, delimiter="\t")
                reader = TabularReader(inner_iterator)
                data = list(reader)

        self.assertEqual(len(data), 3)
        self.assertEqual(data[0], {"planet": "Mercury", "numberOfMoons": "0"})
        self.assertEqual(data[2], {"planet": "Earth", "numberOfMoons": "1"})
