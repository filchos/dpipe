# install needed Python dependencies

install-deps:
	pip3 install -r requirements.dev.txt

# lint and test

test: lint check-types unittest

lint:
	black .
	flake8 --ignore E203,E501 .

check-types:
	mypy --strict .

extended-check-types:
	mypy --strict --python-version=3.10 .
	mypy --strict --python-version=3.11 .
	mypy --strict --python-version=3.12 .
	mypy --strict --python-version=3.13 .

unittest:
	python3 -m pytest tests/ --cov=kojo

show-cov-as_html:
	coverage html
	xdg-open htmlcov/index.html

# publish at PyPi

build-dist:
	python3 -m build

publish:
	twine upload --skip-existing dist/*

